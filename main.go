package main

import (
	"log"
	"os"
	"syscall"

	"os/signal"
	"net/http"

	"bitbucket.org/syariati_faris/ddsample/pkg/inf/monitoring"
	"bitbucket.org/syariati_faris/ddsample/pkg/handler"
	"github.com/facebookgo/grace/gracehttp"
	"github.com/gorilla/mux"
)

func main(){
	initDd()
	monitoring.Run()

	errChan := make(chan error)
	go func() {
		log.Println("starting server..")
		err := gracehttp.Serve(&http.Server{
			Addr: "0.0.0.0:8000",
			Handler: initRouter(),
		})
		errChan <- err
	}()

	term := make(chan os.Signal, 1)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM)
	select {
	case <-term:
		log.Println("signal terminated detected")
	case err := <-errChan:
		log.Println("error detected:", err)
	}
	monitoring.Done()
}

func initRouter()*mux.Router{
	f := handler.NewHandlerFactory()
	router := mux.NewRouter()

	hr := f.GetHelloHandler()
	hr.RegisterHandlers(router)

	return router
}

func initDd(){
	err := monitoring.Init(monitoring.Config{Datadog:monitoring.DatadogConfig{
		AppName: "helloapp",
		AgentAddress: "0.0.0.0:8125",
	}})
	if err != nil{
		log.Panicln("unable to connect to dd, err=",err.Error())
	}
}