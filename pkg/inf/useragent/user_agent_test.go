package useragent

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	UserAgents = map[string]string{
		"opera":      "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41",                //opera
		"chrome":     "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",                                 //chrome
		"google_bot": "Googlebot/2.1 (+http://www.google.com/bot.html)",                                                                                           //google bot
		"safari_ios": "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1", //safari mobile
	}
)

func TestAll(t *testing.T) {
	testNewUserAgentUtil(t)
	testUserAgentUtilImplGetBrowser(t)
	testUserAgentUtilImplGetEngine(t)
	testUserAgentUtilImplGetPlatform(t)
	testUserAgentUtilImplIsBot(t)
	testUserAgentUtilImplGetOS(t)
}

func testNewUserAgentUtil(t *testing.T) {
	for _, ua := range UserAgents {
		GetUserAgentUtil().Parse(ua)
		assert.NotNil(t, GetUserAgentUtil())
	}
}

func testUserAgentUtilImplGetBrowser(t *testing.T) {
	testCases := []struct {
		userAgentData   string
		expectedName    string
		expectedVersion string
	}{
		{
			userAgentData:   UserAgents["opera"],
			expectedName:    "Opera",
			expectedVersion: "38.0.2220.41",
		},
		{
			userAgentData:   UserAgents["safari_ios"],
			expectedName:    "Safari",
			expectedVersion: "10.0",
		},
	}

	for _, tc := range testCases {
		GetUserAgentUtil().Parse(tc.userAgentData)
		name, version := GetUserAgentUtil().GetBrowser()
		assert.Equal(t, name, tc.expectedName)
		assert.Equal(t, version, tc.expectedVersion)
	}
}

func testUserAgentUtilImplGetEngine(t *testing.T) {
	testCases := []struct {
		userAgentData         string
		expectedEngineName    string
		expectedEngineVersion string
	}{
		{
			userAgentData:         UserAgents["chrome"],
			expectedEngineName:    "AppleWebKit",
			expectedEngineVersion: "537.36",
		},
		{
			userAgentData:         UserAgents["opera"],
			expectedEngineName:    "AppleWebKit",
			expectedEngineVersion: "537.36",
		},
	}

	for _, tc := range testCases {
		GetUserAgentUtil().Parse(tc.userAgentData)
		name, version := GetUserAgentUtil().GetEngine()
		assert.Equal(t, name, tc.expectedEngineName)
		assert.Equal(t, version, tc.expectedEngineVersion)
	}
}

func testUserAgentUtilImplGetPlatform(t *testing.T) {
	testCases := []struct {
		userAgentData    string
		expectedPlatform string
	}{
		{
			userAgentData:    UserAgents["chrome"],
			expectedPlatform: "X11",
		},
		{
			userAgentData:    UserAgents["safari_ios"],
			expectedPlatform: "iPhone",
		},
	}

	for _, tc := range testCases {
		GetUserAgentUtil().Parse(tc.userAgentData)
		name := GetUserAgentUtil().GetPlatform()
		assert.Equal(t, name, tc.expectedPlatform)
	}
}

func testUserAgentUtilImplIsBot(t *testing.T) {
	testCases := []struct {
		userAgentData  string
		expectedStatus bool
	}{
		{
			userAgentData:  UserAgents["chrome"],
			expectedStatus: false,
		},
		{
			userAgentData:  UserAgents["google_bot"],
			expectedStatus: true,
		},
	}

	for _, tc := range testCases {
		GetUserAgentUtil().Parse(tc.userAgentData)
		isBot := GetUserAgentUtil().IsBot()
		assert.Equal(t, isBot, tc.expectedStatus)
	}
}

func testUserAgentUtilImplGetOS(t *testing.T) {
	testCases := []struct {
		userAgentData    string
		expectedPlatform string
	}{
		{
			userAgentData:    UserAgents["chrome"],
			expectedPlatform: "Linux x86_64",
		},
		{
			userAgentData:    UserAgents["safari_ios"],
			expectedPlatform: "CPU iPhone OS 10_3_1 like Mac OS X",
		},
	}

	for _, tc := range testCases {
		GetUserAgentUtil().Parse(tc.userAgentData)
		os := GetUserAgentUtil().GetOS()
		assert.Equal(t, os, tc.expectedPlatform)
	}
}
