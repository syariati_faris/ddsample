package useragent

import (
	"github.com/mssola/user_agent"
	"sync"
)

var userAgentUtil UserAgentUtil
var once sync.Once

type UserAgentUtil interface {
	Parse(string)
	GetBrowser() (string, string) //browser name and version
	GetEngine() (string, string)  //engine name and version
	GetPlatform() string
	GetOS() string
	IsBot() bool
}

type UserAgentUtilImpl struct {
	userAgent     *user_agent.UserAgent
	userAgentData string
}

func GetUserAgentUtil() UserAgentUtil {
	once.Do(func() {
		userAgentUtil = newUserAgentUtil("")
	})
	return userAgentUtil
}

func newUserAgentUtil(userAgentData string) UserAgentUtil {
	ua := user_agent.New(userAgentData)
	return &UserAgentUtilImpl{
		userAgentData: userAgentData,
		userAgent:     ua,
	}
}

func (ua *UserAgentUtilImpl) Parse(userAgentData string) {
	ua.userAgent.Parse(userAgentData)
}

func (ua *UserAgentUtilImpl) GetBrowser() (string, string) {
	return ua.userAgent.Browser()
}

func (ua *UserAgentUtilImpl) GetEngine() (string, string) {
	return ua.userAgent.Engine()
}

func (ua *UserAgentUtilImpl) GetPlatform() string {
	return ua.userAgent.Platform()
}

func (ua *UserAgentUtilImpl) IsBot() bool {
	return ua.userAgent.Bot()
}

func (ua *UserAgentUtilImpl) GetOS() string {
	return ua.userAgent.OS()
}
