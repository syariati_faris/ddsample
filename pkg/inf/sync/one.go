package sync

import (
	"sync/atomic"
)

type OneAtATime struct {
	flag int32
}

func (a *OneAtATime) RunAsync(f func()) {
	if a.isRunning() {
		return
	}
	a.setRunning(true)
	go func() {
		defer a.setRunning(false)
		f()
	}()
}

func (a *OneAtATime) setRunning(value bool) {
	var i int32
	if value {
		i = 1
	}
	atomic.StoreInt32(&(a.flag), int32(i))
}

func (a *OneAtATime) isRunning() bool {
	if atomic.LoadInt32(&(a.flag)) != 0 {
		return true
	}
	return false
}
