package sync

import (
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestOneAtATime_Run(t *testing.T) {
	iteration := 10
	var wg sync.WaitGroup
	wg.Add(1)
	oneAtATime := &OneAtATime{}
	count := int32(0)
	otherCount := int32(0)
	incr := func() { atomic.AddInt32(&count, 1) }

	for i := 0; i < iteration; i++ {
		otherCount++
		oneAtATime.RunAsync(func() {
			time.Sleep(time.Duration(iteration) * time.Millisecond)
			incr()
			wg.Done()
		})
	}
	wg.Wait()
	assert.Equal(t, int32(1), count)
}
