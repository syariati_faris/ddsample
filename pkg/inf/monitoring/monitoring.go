//Package monitoring is used for monitoring system.
package monitoring

import (
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"bitbucket.org/syariati_faris/ddsample/pkg/inf/monitoring/datadog"
)

// Datadog structure
type Datadog struct {
	Connection string
}

//Config as monitoring configuration
type Config struct {
	Datadog DatadogConfig
}

type DatadogConfig struct {
	AgentAddress string
	AppName      string
}

type monitor interface {
	Run()
	Done() error
	Timer(float64, []string, string, ...string) error
	Increment(int64, []string, string, ...string) error
	CreateTagName(...string) string
	CreateUserAgentTags(string) []string
}

// Agent for monitoring
var once sync.Once
var agent monitor

func allowed() bool {
	if agent == nil {
		return false
	}
	return true
}

//Init datadog
func Init(cfg Config) error {
	var gerr error
	once.Do(func() {
		if cfg.Datadog.AgentAddress == "" {
			gerr = errors.New("datadog agent address is empty")
			return
		}
		ddAgent, err := datadog.NewDdAgent(datadog.Config{
			AppName: cfg.Datadog.AppName,
			Address: cfg.Datadog.AgentAddress,
		})
		if err != nil {
			gerr = err
			return
		}
		agent = ddAgent
	})
	return gerr
}

// Run monitoring process
func Run() {
	if !allowed() {
		return
	}
	agent.Run()
}

// Done exit monitoring process
func Done() error {
	if !allowed() {
		return nil
	}
	return agent.Done()
}

// Histogram monitoring
func Histogram(env string, duration float64, tags []string, processName string, names ...string) error {
	if !allowed() {
		return nil
	}
	tags = append(tags, fmt.Sprintf("env:%s", env))
	return agent.Timer(duration, tags, processName, names...)
}

// Count monitoring
func Count(env string, count int64, tags []string, processName string, names ...string) error {
	if !allowed() {
		return nil
	}
	tags = append(tags, fmt.Sprintf("env:%s", env))
	return agent.Increment(count, tags, processName, names...)
}

//Create Default Tag Name
func CreateTagName(names ...string) string {
	if !allowed() {
		return ""
	}
	return agent.CreateTagName(names...)
}

//Create User Agent Tah Name
func CreateUserAgentTags(userAgentData string) []string {
	if !allowed() {
		return nil
	}
	return agent.CreateUserAgentTags(userAgentData)
}

//CreateErrorTypeTag creates error type tag
func CreateErrorTypeTag(err error) string {
	if err == nil || !allowed() {
		return ""
	}

	return fmt.Sprintf("%s:%s", "error_type", strings.ToLower(CreateTagName(err.Error())))
}

//Check whether the agent valid
func IsAllowed() bool {
	return allowed()
}

//PublishDatadog publishes histogram and count to datadog
func PublishDatadog(metricName, metricType, env string, startTime time.Time, resultErr error) error {
	duration := time.Since(startTime).Seconds() * 1000
	metricName, tags := createMonitoringTagsAndMetric(metricName, env, resultErr)
	err := Histogram(env, duration, tags, "process_time", metricName, metricType)
	if err != nil {
		return fmt.Errorf("cannot publish datadog historgram, err: %s", err.Error())
	}
	err = Count(env, int64(1), tags, "hit_count", metricName, metricType)
	if err != nil {
		return fmt.Errorf("cannot publish datadog count, err: %s", err.Error())
	}
	return nil
}

//createMonitoringTags creates datadog specific monitoring tags
func createMonitoringTagsAndMetric(metricName string, env string, err error) (string, []string) {
	status := "error"
	if err == nil {
		status = "success"
	}

	tagName := CreateTagName(metricName, status)
	errorTag := CreateErrorTypeTag(err)

	var tags []string
	if tagName != "" {
		tags = append(tags, tagName)
	}

	if errorTag != "" {
		tags = append(tags, errorTag)
	}

	tags = append(tags, status)
	tags = append(tags, env)

	return metricName, tags
}
