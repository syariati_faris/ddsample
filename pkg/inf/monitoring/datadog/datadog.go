// Package datadog is tool for monitoring. This package is used for init datadog agent. Datadog agent is using UDP connection, so connection should be fast.
package datadog

import (
	"errors"
	"strings"

	"bitbucket.org/syariati_faris/ddsample/pkg/inf/useragent"
)

//Config Datadog default configuration
type Config struct {
	Address string
	AppName string
}

//Options of datadog
type Options struct {
	SubName string
	Pretend bool
}

type client interface {
	Histogram(name string, value float64, tags []string, rate float64) error
	Count(name string, value int64, tags []string, rate float64) error
}

const empty = ""

type ddTimer struct {
	name     string
	tags     []string
	duration float64
}

type ddIncrement struct {
	name  string
	tags  []string
	count int64
}

func validate(processName string, tags []string) error {
	if len(processName) == 0 {
		return errors.New("process cannot be empty")
	}
	return nil
}

//CreateUserAgentTags creates new user agent tags for datadog
func CreateUserAgentTags(userAgentData string) []string {
	var tags []string
	useragent.GetUserAgentUtil().Parse(userAgentData)
	browserName, browserVersion := useragent.GetUserAgentUtil().GetBrowser()
	engineName, engineVersion := useragent.GetUserAgentUtil().GetEngine()
	os := useragent.GetUserAgentUtil().GetOS()
	if browserName != empty {
		tags = append(tags, "browser."+strings.ToLower(browserName))
	}
	if browserVersion != empty {
		tags = append(tags, "browser_version."+strings.ToLower(strings.Replace(browserVersion, ".", "_", -1)))
	}
	if engineName != empty {
		tags = append(tags, "browser_engine."+strings.ToLower(engineName))
	}
	if engineVersion != empty {
		tags = append(tags, "browser_engine_version."+strings.ToLower(strings.Replace(engineVersion, ".", "_", -1)))
	}
	if os != empty {
		tags = append(tags, "browser_os."+strings.ToLower(strings.Replace(os, " ", "_", -1)))
	}
	if useragent.GetUserAgentUtil().IsBot() {
		tags = append(tags, "bot")
	}
	return tags
}
