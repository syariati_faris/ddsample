// Package datadog is tool for monitoring. This package is used for init datadog agent. Datadog agent is using UDP connection, so connection should be fast.
package datadog

import (
	"fmt"
	"log"
	"strings"

	"github.com/DataDog/datadog-go/statsd"
	"bitbucket.org/syariati_faris/ddsample/pkg/inf/sync"
)

// Datadog client struct
type ddAgent struct {
	Name       string
	SubName    string
	connection client
	dataQueue  chan interface{}
	oneAtATime *sync.OneAtATime
	//handleDataQueueElement by default is publishDataQueueElement, but it may be replaced with another func for unit test purpose
	handleDataQueueElement func(interface{})
}

// NewAgent Datadog object
func NewDdAgent(cfg Config, opts ...Options) (*ddAgent, error) {
	var opt Options
	if len(opts) > 0 {
		opt = opts[0]
	}
	var (
		connection *statsd.Client
		err        error
	)
	if !opt.Pretend {
		connection, err = statsd.New(cfg.Address)
		if err != nil {
			return nil, err
		}
	}
	d := &ddAgent{
		Name:       cfg.AppName,
		SubName:    opt.SubName,
		connection: connection,
		dataQueue:  make(chan interface{}, 30), // buffer size 30 means there will be 30 thread workers. @see ddagent.process
		oneAtATime: &sync.OneAtATime{},
	}
	d.handleDataQueueElement = d.publishDataQueueElement
	return d, nil
}

func (d *ddAgent) structurizedName(processname string, names ...string) string {
	var (
		name, prefix, subname string
	)
	name = d.Name
	subname = d.SubName

	if len(names) > 0 {
		subname = names[0]
		if len(names) >= 2 {
			prefix = names[1]
		}
		if len(names) >= 3 && names[2] != "" {
			name = names[2]
		}
	}
	r := strings.NewReplacer(
		"http://", "http_request.",
		"https://", "http_request.",
		"www.", "",
		"/", "_",
		":", "",
		"{{.", "",
		"}}", "",
	)
	subname = r.Replace(subname)

	if prefix != "" {
		name += fmt.Sprintf(".%s", prefix)
	}
	return fmt.Sprintf("%s.%s.%s", name, subname, processname)
}

func (d *ddAgent) CreateTagName(names ...string) string {
	return createTagName(names...)
}

func (d *ddAgent) CreateUserAgentTags(userAgentData string) []string {
	return CreateUserAgentTags(userAgentData)
}

// Run datadog golang agent, empty func just to comply monitoring interface
func (d *ddAgent) Run() {
	log.Println("starting datadog..")
}

// Done exit datadog agent, empty func just to comply monitoring interface
func (d *ddAgent) Done() error {
	close(d.dataQueue)
	return nil
}

func (d *ddAgent) process() {
	d.oneAtATime.RunAsync(func() {
		for {
			select {
			case data, ok := <-d.dataQueue:
				if ok && data != nil {
					go func() {
						d.handleDataQueueElement(data) // in real implementation will call publishDataQueueElement
					}()
				}
				if !ok {
					return
				}
			default:
				return
			}
		}
	})
}

func (d *ddAgent) publishDataQueueElement(data interface{}) {
	log.Println(data)
	switch data.(type) {
	case ddTimer:
		timer := data.(ddTimer)
		if err := d.connection.Histogram(timer.name, timer.duration, timer.tags, 1); err != nil {
			log.Printf("unable to push histogram to dd: err=%s, tags=%+v\n", err.Error(), timer.tags)
		}
	case ddIncrement:
		increment := data.(ddIncrement)
		if err := d.connection.Count(increment.name, increment.count, increment.tags, 1); err != nil {
			log.Printf("unable to push increment to dd: err=%s, tags=%+v\n", err.Error(), increment.tags)
		}
	}
}

// Timer datadog
func (d *ddAgent) Timer(duration float64, tags []string, processName string, names ...string) error {
	err := validate(processName, tags)
	if err != nil {
		return err
	}
	trueName := d.structurizedName(processName, names...)
	d.pushData(ddTimer{
		name:     trueName,
		tags:     tags,
		duration: duration,
	})
	return nil
}

// Increment datadog
func (d *ddAgent) Increment(count int64, tags []string, processName string, names ...string) error {
	err := validate(processName, tags)
	if err != nil {
		return err
	}

	trueName := d.structurizedName(processName, names...)
	d.pushData(ddIncrement{
		name:  trueName,
		tags:  tags,
		count: count,
	})

	return nil
}

func (d *ddAgent) pushData(data interface{}) {
	d.dataQueue <- data
	d.process()
}

func createTagName(names ...string) string {
	var (
		action, status string
	)

	if len(names) > 0 {
		action = names[0]
		if len(names) > 1 {
			status = names[1]
		}
	}

	r := strings.NewReplacer(
		"/", "_",
		" ", "_",
	)

	action = r.Replace(action)
	return fmt.Sprintf("%s%s", status, action)
}
