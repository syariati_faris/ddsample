package datadog

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateUserAgentTags(t *testing.T) {
	userAgentData := "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"
	expectedTags := []string{
		"browser.chrome",
		"browser_version.23_0_1271_97",
		"browser_engine.applewebkit",
		"browser_engine_version.537_11",
		"browser_os.linux_x86_64",
	}
	dd, err := NewDdAgent(Config{Address: "something", AppName: "cartapp"}, Options{Pretend: false})
	assert.Error(t, err)
	userAgentTags := dd.CreateUserAgentTags(userAgentData)
	assert.Equal(t, expectedTags, userAgentTags)
}
