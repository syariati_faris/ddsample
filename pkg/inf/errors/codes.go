package errors

import (
	"errors"
	"net/http"
)

// Codes of error
type Codes uint8

// error list
const (
	// Unclassified error.
	Other Codes = iota
	//Bad Request error
	BadRequest
	//UnAuthorized error
	UnAuthorized
	//Internal Server error
	InternalServerError
)

// GetErrorAndCode func
func (c Codes) GetErrorAndCode() (string, int) {
	switch c {
	case BadRequest:
		return "bad request", http.StatusBadRequest
	case UnAuthorized:
		return "unauthorized user", http.StatusUnauthorized
	case Other:
		return "internal server error", http.StatusInternalServerError
	default:
		return "internal server error", http.StatusInternalServerError
	}
}

// Err return standard error type
func (c Codes) Err() error {
	errString, _ := c.GetErrorAndCode()
	return errors.New(errString)
}

func (c Codes) GetErrAndCode() (error, int) {
	errString, code := c.GetErrorAndCode()
	return errors.New(errString), code
}
