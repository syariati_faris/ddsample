package errors

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetErrorAndCode(t *testing.T) {
	cases := []struct {
		codes        Codes
		expectString string
		expectInt    int
	}{
		{
			codes:        InternalServerError,
			expectString: "internal server error",
			expectInt:    http.StatusInternalServerError,
		},
		{
			codes:        BadRequest,
			expectString: "bad request",
			expectInt:    http.StatusBadRequest,
		},
	}

	for _, tc := range cases {
		actualString, actualInt := tc.codes.GetErrorAndCode()
		assert.Equal(t, tc.expectString, actualString)
		assert.Equal(t, tc.expectInt, actualInt)
	}
}

func TestErr(t *testing.T) {
	code := new(Codes)

	err := code.Err()
	if err == nil {
		t.Fatal("No Error Found")
	}
}

func TestGetErrAndCodeErr(t *testing.T) {
	code := new(Codes)

	err, _ := code.GetErrAndCode()
	if err == nil {
		t.Fatal("No Error Found")
	}
}
