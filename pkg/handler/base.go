package handler

import (
	"bitbucket.org/syariati_faris/ddsample/pkg/inf/errors"
	"bitbucket.org/syariati_faris/ddsample/pkg/inf/monitoring"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type IHandler interface{
	RegisterHandlers(router *mux.Router)
	Name()string
}

type baseHandler struct{}

type bfunc func(writer http.ResponseWriter, request *http.Request)(interface{}, error)

func (b *baseHandler) WrapDD(bf bfunc)http.HandlerFunc{
	return func(writer http.ResponseWriter, request *http.Request) {
		start := time.Now()
		data, err := bf(writer, request)
		write(writer, data, err)
		go monitoring.PublishDatadog(request.URL.Host+request.URL.Path, "rest_handler", "development",  start, err)
	}
}

func write(w http.ResponseWriter, data interface{}, err error){
	str, status := errors.ErrorAndHTTPCode(err)
	w.WriteHeader(status)
	w.Write([]byte(fmt.Sprint("data=", data, " err=", str)))
}