package handler

type IHandlerFactory interface{
	GetHelloHandler() IHandler
}

func NewHandlerFactory() IHandlerFactory{
	return &handlerFactory{
		b: &baseHandler{},
	}
}

type handlerFactory struct{
	b *baseHandler
}

func (h *handlerFactory) GetHelloHandler()IHandler{
	return &helloHandler{
		baseHandler: h.b,
	}
}
