package handler

import (
	"bitbucket.org/syariati_faris/ddsample/pkg/inf/errors"
	"github.com/gorilla/mux"
	"net/http"
)

type helloHandler struct{
	*baseHandler
}

func (h *helloHandler) RegisterHandlers(router *mux.Router) {
	router.HandleFunc("/v1/hello", h.WrapDD(h.Hello)).Methods(http.MethodGet)
}

func (*helloHandler) Name() string {
	return "HelloHandler"
}

func (h *helloHandler) Hello(w http.ResponseWriter, r *http.Request)(interface{}, error){
	//lets do logic here for test
	err := errors.New(errors.BadRequest)
	return "hello world", err
}


